﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
public class pathmanager : MonoBehaviour
{


    public static pathmanager Inst;

    public List<GameObject> PathList = new List<GameObject>();

    // The event to fire while the button is held
    public UnityEvent OnButtonHeld;

    // 2
    // This is the state of our button (is it being pressed?)
    private bool pressed = false;

    bool MoveElectron;
    [SerializeField]
    GameObject Electron;
    [SerializeField]
    float timeBetweenSpawns;

    private void Awake()
    {
        Inst = this;
        DontDestroyOnLoad(this);

    }

    private void OnEnable()
    {
        
        EventManager.onButtonClick += OnPressButton;
        EventManager.onButtonRelease += OnReleaseButton;
       
        
    }

    void OnPressButton()
    {
        // OnPress Button
//        Debug.Log("Button Pressed");
        pressed = true;
        MoveElectron = true;
        StartCoroutine(WaitABit());
    }

    void OnReleaseButton()
    {
        // Release Button
      //  Debug.Log("Button Release");
        pressed = false;
        MoveElectron = false;
        StopAllCoroutines();
    }
    // Start is called before the first frame update
    void Start()
    {
        MoveElectron = false;
        timeBetweenSpawns = 0.1f;


        
    }
    public void SetPressed(bool value)
    {
        pressed = value;
    }



    // 2
    // If the button's state is pressed then we fire the
    // OnButtonHeld event.
    private void Update()
    {
        if (pressed)
            OnButtonHeld.Invoke();
    }
   /* public void OnPointerDown(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }*/



    IEnumerator WaitABit()
    {

       // Debug.Log("Coroutine Called....."+ MoveElectron);
        //Spawn as many (clues?) as you nee
        while (MoveElectron)
        {
          //  Debug.Log("Spwan.....");
            Instantiate(Electron, PathList[0].transform.position, Quaternion.identity);
            //Wait awhile before spawning new items
            yield return new WaitForSeconds(timeBetweenSpawns);
        }
    }
}
