﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public delegate void OnButtonClick();
    public delegate void OnButtonRelease();
    public static event OnButtonClick onButtonClick;
    public static event OnButtonRelease onButtonRelease;
    public void RaiseOnButtonClick()
    {
        if (onButtonClick != null)
        {
            onButtonClick();
        }
    }

    public void ReleaseButtonClick()
    {
        if (onButtonRelease != null)
        {
            onButtonRelease();
        }
    }
}

