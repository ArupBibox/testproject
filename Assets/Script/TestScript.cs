﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class TestScript : MonoBehaviour
{
    [SerializeField]
    float step;

    private void Awake()
    {
        StartCoroutine(MoveElectronCoroutine());
    }

    void MovePlayer1()
    {
    //    StartCoroutine(MoveElectronCoroutine());
    }

    IEnumerator MoveElectronCoroutine()
    {
        Debug.Log("pathmanager.Inst.PathList...."+ pathmanager.Inst.PathList.Count);
        foreach (GameObject _item in pathmanager.Inst.PathList)
        {
            Debug.Log("Moving....");
            Vector3 itemPos = _item.transform.position;
            while (Vector3.Distance(transform.position, itemPos) > .0001)
            {
                transform.position = Vector3.MoveTowards(transform.position, itemPos, step * Time.deltaTime);
                if(Vector3.Distance(
                    transform.position, pathmanager.Inst.PathList[pathmanager.Inst.PathList.Count - 1].transform.position) < 0.01f)
                {
                    Destroy(this.transform.gameObject);
                }
                yield return null;
            }
        }
    }
}

